# Описание

Данный репозиторий содержит набор DevSecOps задач, которые можно легко интегрировать в любой проект.

В [.gitlab/scripts](.gitlab/scripts) находятся скрипты, которые необходимы для визуализации уязвимостей в выводе задачи CI/CD

В [.gitlab/templates](.gitlab/templates) находятся шаблоны конкретных задач

В [examples](.examples) лежат примеры файлов с различными уязвимостями

В [.gitlab-ci.yml](.gitlab-ci.yml) в блоке include происходит подключение шаблонов из [.gitlab/templates](.gitlab/templates), можно таким образом включать только те задачи, которые нужны

# Полезные ссылки

## Pre-commit Checks

OWASP описание: https://owasp.org/www-project-devsecops-guideline/latest/01a-Secrets-Management

### gitLeaks

Gitleaks: https://github.com/zricethezav/gitleaks

Конфиг gitLeaks в GitLab: https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml

Описание Secret Detection: https://docs.gitlab.com/ee/user/application_security/secret_detection/

Шаблон Secret Detection: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml

## Commit-time Checks

OWASP описание: https://owasp.org/www-project-devsecops-guideline/latest/02a-Static-Application-Security-Testing

### SAST

Список анализаторов: https://owasp.org/www-community/Source_Code_Analysis_Tools

Описание SAST в GitLab: https://docs.gitlab.com/ee/user/application_security/sast/

Шаблон SAST: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml

Коллекция кода с уязвимостями: https://github.com/snoopysecurity/Vulnerable-Code-Snippets

#### SonarQube

[Инструкция по настройке](https://gitlab.com/spectr-devsecops/sonarqube)

### Dependency Scanning

Описание в GitLab: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/

### Gemnasium

Репозиторий: https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium

База уязвимостей: https://advisories.gitlab.com/

## Post-build Checks

OWASP описание Container Scanning: https://owasp.org/www-project-devsecops-guideline/latest/02f-Container-Vulnerability-Scanning

Описание Container Scanning в GitLab: https://docs.gitlab.com/ee/user/application_security/container_scanning/

Исходники: https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning

Trivy: https://aquasecurity.github.io/trivy/v0.32/tutorials/integrations/gitlab-ci/

## Test-time Checks

OWASP описание DAST: https://owasp.org/www-project-devsecops-guideline/latest/02b-Dynamic-Application-Security-Testing

DAST в GitLab: https://docs.gitlab.com/ee/user/application_security/dast/

OWASP ZAP: https://www.zaproxy.org/

ZAP - API Scan: https://www.zaproxy.org/docs/docker/api-scan/

## Deploy-time checks

OWASP описание IAST: https://owasp.org/www-project-devsecops-guideline/latest/02c-Interactive-Application-Security-Testing

OWASP описание Infrastructure Scanning: https://owasp.org/www-project-devsecops-guideline/latest/02e-Infrastructure-Vulnerability-Scanning

IaC Scanning: https://docs.gitlab.com/ee/user/application_security/iac_scanning/

## Ещё ссылки

Шаблоны заданий CI/CD тут: https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Jobs

Отчеты гитлаба: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/reports.gitlab-ci.yml

Шаблон автодевопс: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml

Продукты гитлаба: https://gitlab.com/gitlab-org/security-products

OWASP WebGoat: https://owasp.org/www-project-webgoat/

Образы WebGoat: https://hub.docker.com/u/webgoat

Пример написания отчетов уязвимостей в комментариях к реквестам: https://about.gitlab.com/blog/2022/02/17/fantastic-infrastructure-as-code-security-attacks-and-how-to-find-them/