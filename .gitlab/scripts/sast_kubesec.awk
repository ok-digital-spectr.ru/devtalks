#!/bin/awk -f

BEGIN {
    FS=":"
    RS=",|\n"
}

/^\s*"vulnerabilities"/ {
    vulnerabilities=1
}

/^\s*"severity"/ && vulnerabilities {
    severity[++count]=$2
    gsub(/"/, "", severity[count])
}

/^\s*"name"/ && vulnerabilities {
    name[count]=$2
    gsub(/"/, "", name[count])
}

/^\s*"location".*"file"/ && vulnerabilities {
    file[count]=$NF
    gsub(/"/, "", file[count])
}

END {
    if (vulnerabilities) {
        printf "|     Severity     |     Name     |     File     |\n"
        printf "|------------------|--------------|--------------|\n"
        for (i=1; i<=count; i++) {
            printf "| %s | %s | %s |\n", gensub(/"/, "\"\"", "g", severity[i]), gensub(/"/, "\"\"", "g", name[i]), gensub(/"/, "\"\"", "g", file[i])
        }
        printf "Number of vulnerabilities: %d\n", count
    }
    exit count
}



