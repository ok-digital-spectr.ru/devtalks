#!/bin/bash

EXCLUSION_FILE=".gitlab/sast-ruleset.toml"
if [ -f "$EXCLUSION_FILE" ]; then
  EXCLUSION_LIST=$(toml2json < "$EXCLUSION_FILE" | jq -c '.kics.ruleset[]')
else
  EXCLUSION_LIST=""
fi

echo $EXCLUSION_LIST

# Функция проверки, является ли уязвимость исключением
is_excluded() {
  local type="$1"
  local value="$2"

  for exclusion in $EXCLUSION_LIST; do
    exclude_type=$(echo "$exclusion" | jq -r '.identifier.type')
    exclude_value=$(echo "$exclusion" | jq -r '.identifier.value')

    if [[ "$exclude_type" == "$type" && "$exclude_value" == "$value" ]]; then
      return 0
    fi
  done

  return 1
}

# Подсчет уязвимостей с учетом исключений
vulnerability_count=0
for row in $(cat $FILE_REPORT | jq -r '.vulnerabilities[] | @base64'); do
  _jq() {
    echo ${row} | base64 --decode | jq -r ${1}
  }

  identifier_type=$(_jq ".identifiers[0].type")
  identifier_value=$(_jq ".identifiers[0].value")

  if ! is_excluded "$identifier_type" "$identifier_value"; then
    ((vulnerability_count++))
  fi
done


# Вывод уязвимостей, если они есть
if [ ${vulnerability_count} -gt 0 ]; then
  echo "|     severity     |     name     |     location     |"
  echo "|------------------|--------------|------------------|"
  for row in $(cat $FILE_REPORT | jq -r '.vulnerabilities[] | @base64'); do
    _jq() {
      echo ${row} | base64 --decode | jq -r ${1}
    }

    identifier_type=$(_jq ".identifiers[0].type")
    identifier_value=$(_jq ".identifiers[0].value")

    if ! is_excluded "$identifier_type" "$identifier_value"; then
      echo '|' $(_jq ".severity") '|' $(_jq ".identifiers[0].name") '|' $(_jq ".location.file")':'$(_jq ".location.start_line") '|'
    fi
  done
fi

exit ${vulnerability_count}